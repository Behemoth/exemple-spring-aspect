package io.vn.controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "*", maxAge = 36000)
@RestController
public class AlertAction {

	@RequestMapping(value = "/alert", method = RequestMethod.GET)
	public void actionAlert() throws InterruptedException {
		System.out.println("alert");
		Thread.sleep(150);
	}

}
