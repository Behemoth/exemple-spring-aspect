package io.vn.util;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.context.annotation.Configuration;

/**
 * 
 * @author vn
 */
@Aspect
@Configuration
public class LogginAspect {

	/**
	 * Logger LOG4J
	 */
	 private static final Logger LOGGER = LogManager.getLogger();
	
	long time;
	
	/**
	 * Méthode de log avant fonction
	 * 
	 * @param joinPoint
	 */
	@Before("execution(* io.vn.*.*.*(..))")
	private void logMethodEntry(JoinPoint joinPoint) {
		LOGGER.info("{} - {} - Début", joinPoint.getSignature().getDeclaringTypeName(), joinPoint.getSignature().getName());
		time = System.currentTimeMillis();
	}
	
	/**
	 * Méthode de log après fonction
	 * 
	 * @param joinPoint
	 */
	@After("execution(* io.vn.*.*.*(..))")
	private	 void logMethodExit(JoinPoint joinPoint) {
		LOGGER.info("{} - {} - Fin - Temps d'éxécution : {}ms", joinPoint.getSignature().getDeclaringTypeName(), joinPoint.getSignature().getName(), System.currentTimeMillis() - time);
	}

}
